import React from 'react';
import Link from 'next/link'

const Footer = () => (
  <>
  <footer>
    <Link href="/">
      <a>© 2020 Mejor con Salud | Revista sobre buenos hábitos y cuidados para tu salud</a>
    </Link>
  </footer>
  <style jsx>{`
    footer{
      padding: 1rem;
      background-color: var(--primary-lime);
      text-align: center;
      a{
        display: inline-block;
        font-size: 0.875rem;
        color: var(--absolute-light);
        padding:0.5rem;          
        transition: all 0.3s;
        &:hover{
          color: var(--primary-yellow);
        }
      }
    }
  `}</style>
  </>
);

export default Footer;
