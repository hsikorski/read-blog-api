import React from 'react';
import Link from 'next/link'

const Header = () => (
  <>    
    <header>
      <Link href="/">
        <a><img src="http://localhost:9045/img/header-brand-mcs-logo.svg" alt="Site Logo" /></a>
      </Link>
      <input type="search" name="search" placeholder="Search" />
    </header>
    <style jsx>{`
      header{
        padding: 1rem;
        background-color: var(--absolute-light);
        display: flex;
        align-items: center;
        justify-content: space-between;
        a{
          display: inline-block;
        }
        img{
          width:200px;
        }
      }
    `}</style>
  </>
);

export default Header;
