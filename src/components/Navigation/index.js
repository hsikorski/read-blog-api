import React from 'react';
import Link from 'next/link'

const Navigation = () => (
  <>
    <nav>
      <ul>
        <li>
          <Link href="/">
            <a>Home</a>
          </Link>
        </li>
        <li>
          <Link href="/posts">
            <a>Posts</a>
          </Link>
        </li>
        <li>
          <Link href="/authors">
            <a>Authors</a>
          </Link>
        </li>
      </ul>
    </nav>
    <style jsx>{`
      nav{
        display:flex;
        background-color: var(--primary-blue);
        justify-content: flex-end;
        align-items: center;
        ul{
          padding:0.75rem 0.5rem;
        }
        li{
          display: inline-block;
        }
        a{
          display: inline-block;
          color: var(--absolute-light);
          padding:0.5rem;          
          transition: all 0.3s;
          &:hover{
            color: var(--primary-yellow);
          }
        }
      }
    `}</style>
  </>
);

export default Navigation;
