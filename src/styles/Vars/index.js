
import css from 'styled-jsx/css';

export default css.global`
  // Colors
  $primary-green: #4A8282;
  $primary-blue: #4AB3E4;
  $primary-pink: #F2627D;
  $primary-yellow: #FAB94D;
  $primary-lime: #94CD71;
  $primary-grey: #6A6C6F;

  $main-background-color: #fbfcfd;
  $absolute-light: #ffffff;
  $absolute-dark: #000000;

  @function hexToRGB($hex) {
    @return red($hex), green($hex), blue($hex);
  }

  :root {
  // Colors
    --primary-green: #{$primary-green};
    --primary-blue: #{$primary-blue};
    --primary-pink: #{$primary-pink};
    --primary-yellow: #{$primary-yellow};
    --primary-lime: #{$primary-lime};
    --primary-grey: #{$primary-grey};

    --main-background-color: #{$main-background-color};
    --absolute-light: #{$absolute-light};
    --absolute-dark: #{$absolute-dark};

    // Darker variations:
    --primary-green-darker: #{scale-color($primary-green, $lightness: -80%)};
    --primary-blue-darker: #{scale-color($primary-blue, $lightness: -50%)};
    --primary-pink-darker: #{scale-color($primary-pink, $lightness: -80%)};
    --primary-yellow-darker: #{scale-color($primary-yellow, $lightness: -80%)};
    --primary-lime-darker: #{scale-color($primary-lime, $lightness: -80%)};
    --primary-grey-darker: #{scale-color($primary-grey, $lightness: -80%)};

    //Lighter variations:
    --primary-green-lighter: #{scale-color($primary-green, $lightness: 80%)};
    --primary-blue-lighter: #{scale-color($primary-blue, $lightness: 80%)};
    --primary-pink-lighter: #{scale-color($primary-pink, $lightness: 80%)};
    --primary-yellow-lighter: #{scale-color($primary-yellow, $lightness: 80%)};
    --primary-lime-lighter: #{scale-color($primary-lime, $lightness: 80%)};
    --primary-grey-lighter: #{scale-color($primary-grey, $lightness: 80%)};

    --primary-green-rgb: #{hexToRGB($primary-green)};
    --primary-blue-rgb: #{hexToRGB($primary-blue)};
    --primary-pink-rgb: #{hexToRGB($primary-pink)};
    --primary-yellow-rgb: #{hexToRGB($primary-yellow)};
    --primary-lime-rgb: #{hexToRGB($primary-lime)};
    --primary-grey-rgb: #{hexToRGB($primary-grey)};

    --absolute-light-rgb: #{hexToRGB($absolute-light)};
    --absolute-light-rgb: #{hexToRGB($absolute-light)};

    // Spacing:
    --root-base-font-size: 16px;
    --root-base-size: 8px;
    --root-base-min-size: 48px;
    --root-base-multiplier: 1;

    // SFx:
    --default-shadow: 0px 4px 24px rgba(0, 0, 0, 0.08);
  }
`;
