import React from 'react';

import reset from '../Reset';
import vars from '../Vars';

/**
 * GlobalStyles.js
 */
function GlobalStyles() {
  return (
    <React.Fragment>
      <style jsx>{reset}</style>
      <style jsx>{vars}</style>
      <style global jsx>
        {`
        * {
          box-sizing: border-box;
        }

        html, body, #__next {
          padding: 0;
          margin: 0;
          width: 100%;
          height: 100%;
          font-family: 'Roboto', Arial, Helvetica, sans-serif;
        }

        h1, h2, h3, h4, h5, h6 {
          font-family: 'Roboto', Arial, Helvetica, sans-serif;
        }

        body {
          background-color: var(--main-background-color);
          color: var(--main-text-color);
          font-size:16px;          
        }

        #root{
          overflow: hidden;
        }

        .icon {
          min-height: var(--root-base-min-size);
          min-width: var(--root-base-min-size);
          box-sizing: border-box;
          display: inline-flex;
          align-items: center;
          justify-content: center;

          svg {
            height: calc(var(--root-base-size) * 3);
            width: auto;
          }
         }

         p  { font-size: calc(var(--root-base-font-size) * var(--root-base-multiplier)); line-height: 1.5em;}
         
         a  { text-decoration:none; }
         
         h1 { font-size: calc(var(--root-base-font-size) * var(--root-base-multiplier) * 2.5); line-height: 1.2em; font-weight: 700; }
         h2 { font-size: calc(var(--root-base-font-size) * var(--root-base-multiplier) * 2); line-height: 1.5em;  font-weight: 700; }
         h3 { font-size: calc(var(--root-base-font-size) * var(--root-base-multiplier) * 1.5); line-height: 1.333333;  font-weight: 700; }
         h4 { font-size: calc(var(--root-base-font-size) * var(--root-base-multiplier) * 1.25); line-height: 1.6; font-weight: 700; }
         h5 { font-size: calc(var(--root-base-font-size) * var(--root-base-multiplier) * 1); line-height: 2em; font-weight: 700;}
         h6 { font-size: calc(var(--root-base-font-size) * var(--root-base-multiplier) * 1); line-height: 2em;  font-weight: 300;}
      `}
      </style>
    </React.Fragment>
  );
}

export default GlobalStyles;