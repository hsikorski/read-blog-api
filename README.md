## Install

On first time
```
yarn init -y

yarn add next react react-dom

yarn add axios

yarn add next-routes

yarn add sass

yarn add @zeit/next-sass node-sass

yarn add styled-jsx

yarn add styled-jsx-plugin-sass

yarn add styled-jsx-themes

yarn add sass-loader

```

## Config
On first time
```
"scripts": {
  "dev": "next -p 9045",
  "start": "next start",
  "build": "next build" 
}
```

## To Run
```
yarn dev
```