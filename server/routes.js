const routes = require('next-routes');

module.exports = routes()
  .add('/','/Home')
  .add('/home','/Home')
  .add('/posts','/Posts')
  .add('/post/:post','/Post')
  .add('/authors','/Authors')
  .add('/author/:author','/Author')
  .add('/search/:query','/Search');