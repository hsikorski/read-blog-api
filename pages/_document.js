import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';

// styles
import GlobalStyles from '../src/styles/GlobalStyles';

// components
import Header from '../src/components/Header';
import Nav from '../src/components/Navigation';
import Footer from '../src/components/Footer';

export default class AppDocument extends Document {
  render(){
    return(
      <html>
        <Head>
          <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
          <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.3/build/grids-min.css" />
          <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.3/build/grids-responsive-min.css" />
          <GlobalStyles />
        </Head>
        <body>
          <div id="root">
            <Header />
            <Nav />
            <Main />
            <Footer />
            <NextScript />
          </div>
        </body>
      </html>
    );
  }
}