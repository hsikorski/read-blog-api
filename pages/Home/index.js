import React from 'react';
import axios from 'axios';
import Link from 'next/link'

const Home = ({ posts }) => (
  <div className="pure-g">
    { posts.map(post => (
      <div className="pure-u-1 pure-u-md-1-2 pure-u-lg-1-3" key={post.id}>
        <div className="card">
          <h4>
            <Link href={`/post/${post.id}`}>
              <a>{post.title}</a>
            </Link>
          </h4>
          <figure>
            <Link href={`/post/${post.id}`}>
              <a><img src={post.featured_media ? post.featured_media.medium : './img/no-image.jpg'} alt={`Thumb for post ${post.title}`} /></a>
            </Link>
          </figure>
          <p>
            <Link href={`/post/${post.id}`}>
              <a>{post.excerpt.replace('&hellip;','')} [...]</a>   
            </Link>
          </p>
        </div>
      </div>
    )) }    
    <style jsx>{`
      .container{
        padding:1rem;
      }
      .card{
        border-radius: 10px;
        background-color: var(--absolute-light);
        box-shadow:var(--default-shadow);
        padding:1rem;
        margin:0.5rem;
        transition: all 0.2s;
        border: 1px solid var(--primary-grey-lighter);
        &:hover{
          opacity: 0.5;
        }
        a{
          text-decoration: none;
          color: var(--primary-grey);
          display: block;
        }
        figure{
          height:150px;
          overflow: hidden;
          position: relative;          
          img{
            width:100%;
            margin-top: 10px;
          }
        }
        h4{
          line-height: 1em;
        }
        p{
          margin:1rem 0;
        }
      }
    `}</style>  
  </div>
);

Home.getInitialProps = async () => {
  const response = await axios.get(
    'https://api.beta.mejorconsalud.com/wp-json/mc/v1/posts?orderby=relevance'
  );
  return { posts: response.data };
}

export default Home;