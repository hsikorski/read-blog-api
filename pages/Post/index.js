import React from 'react';
import axios from 'axios';

import Head from 'next/head';

const Post = ({ post }) => (
  <div className="pure-g"> 
    <Head>
      {/* metas  and schema*/}
    </Head>
    <div className="pure-u-1 pure-u-md-1-2 pure-u-lg-2-3">
      <h1>{post.title}</h1>
      <span>data: {post.published}</span>
      <p>tags:
      {post.tags.map(tag => (
        <span key={tag.id}>&nbsp; {tag.name} &nbsp;</span>
      ))
      }
      </p>
      <span>autor: {post.author.name}</span>
      <figure>
        <img src={post.featured_media.large} alt={` ${post.title}`} />
      </figure>
      <div dangerouslySetInnerHTML={{ __html: post.content }} />
      <hr />
      <h6>Sobre o autor</h6>
      <h5>{post.author.name}</h5>
      <div dangerouslySetInnerHTML={{ __html: post.author.description }} />
      <img src={post.author.picture} alt={`${post.author.name}`} />
      <hr />
    </div> 
    <div className="pure-u-1 pure-u-md-1-2 pure-u-lg-1-3">
      <h6>Categorias</h6>
      <ul>
      </ul>
      <hr />
      <img src="https://s0.2mdn.net/simgad/656490617621746830" alt="Advertisement" border="0" width="300" height="600" />
    </div> 
    <div className="pure-u-1 ">
      <h5>Outros posts</h5>
      {post.related_links.map(otherPost => (
        <h4 key={otherPost.id}>&nbsp; {otherPost.title} &nbsp;</h4>
      ))
      }
    </div>
    <style jsx>{`
      
    `}</style>  
  </div>
);

Post.getInitialProps = async ({ query}) => {
  const response = await axios.get(
    `https://api.beta.mejorconsalud.com/wp-json/mc/v1/posts/${query.post}`
  );
  return { post: response.data };
}

export default Post;